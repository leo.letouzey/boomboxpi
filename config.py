class Config:
    DATA = "/home/pi/boomboxpi/data/playlists/"
    TRACKS_FOLDER = DATA + "tracks/"
    DUMP_FOLDER = DATA + "dump/"
    TRACKS_IMG_FOLDER = DATA + "img/tracks/"
    PLAYLISTS_IMG_FOLDER = DATA + "img/playlists/"
